package server;

/**
 * Imports
 */
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;

import marionettes.Expressions;

public class Server {

	/**
	 * Variables
	 */
	private static int PORT; // Port Number
	private static Properties prop = new Properties(); // Properties File
	private static FileInputStream input; // Input
	private static boolean useGUI = false; // Whether or not to use GUI
	private static ServerGUI gui; // GUI

	/**
	 * The set of all names of clients
	 */
	private static HashSet<String> names = new HashSet<String>();

	/**
	 * The set of all the print writers for all the clients. This set is kept so
	 * we can easily broadcast messages.
	 */
	private static HashSet<PrintWriter> writers = new HashSet<PrintWriter>();
	// the set of all the xcoords for the users, for syncing when a new client
	// connects
	private static HashMap<String, Integer> xcoords = new HashMap<String, Integer>();
	// what puppet each client has selected, for syncing
	private static HashMap<String, Integer> puppetNums = new HashMap<String, Integer>();
	// so everyone sees the die land in the same position
	private static int dieNum = 0;
	private static HashMap<String, String> mouthNums = new HashMap<String, String>();
	private static HashMap<String, String> browNums = new HashMap<String, String>();
	private static HashMap<String, Integer> facings = new HashMap<String, Integer>();
	private static HashMap<String, ArrayList<Integer>> hats = new HashMap<String, ArrayList<Integer>>();
	private static HashMap<String, Integer> bodies = new HashMap<String, Integer>();
	private static HashMap<String, String> displayHands = new HashMap<String, String>();
	private static HashMap<String, String> currentHands = new HashMap<String, String>();
	private static HashMap<String, String> handFrames = new HashMap<String, String>();
	private static HashMap<String, String> configStrings = new HashMap<String,String>();

	/**
	 * Main Method for Server - Loads properties and starts the server.
	 */
	public static void main(String[] args) throws IOException {
		try {
			input = new FileInputStream("resources/config.properties");

			// loads properties file
			prop.load(input);

			String guiString = prop.getProperty("useGui");
			String portString = prop.getProperty("PortNumber", "9001");
			PORT = Integer.parseInt(portString);
			useGUI = Boolean.parseBoolean(guiString);

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		// if the gui is in use, log it to that, else print it
		// mostly for running on a vps
		if (useGUI) {
			gui = new ServerGUI();
		}
		if (gui != null) {
			gui.log("Marionette Mates server running on port " + PORT + ".");
		} else {
			System.out.println("Marionette Mates server running on port "
					+ PORT + ".");
		}
		ServerSocket listener = new ServerSocket(PORT);
		try {
			while (true) {
				new Handler(listener.accept()).start();
			}
		} finally {
			listener.close();
		}

	}

	/**
	 * A handler thread class. Handlers are spawned from the listening loop and
	 * are responsible for a dealing with a single client and broadcasting its
	 * messages.
	 */
	private static class Handler extends Thread {
		private String name;
		private Socket socket;
		private BufferedReader in;
		private PrintWriter out;

		/**
		 * Constructs a handler thread, squirreling away the socket. All the
		 * interesting work is done in the run method.
		 */
		public Handler(Socket socket) {
			this.socket = socket;
		}

		/**
		 * Services this thread's client by repeatedly requesting a screen name
		 * until a unique one has been submitted, then acknowledges the name and
		 * registers the output stream for the client in a global set, then
		 * repeatedly gets inputs and broadcasts them.
		 */
		public void run() {
			try {

				// Create character streams for the socket.
				in = new BufferedReader(new InputStreamReader(
						socket.getInputStream()));
				out = new PrintWriter(socket.getOutputStream(), true);

				// Request a name from this client. Keep requesting until
				// a name is submitted that is not already used. Note that
				// checking for the existence of a name and adding the name
				// must be done while locking the set of names.
				while (true) {
					out.println("SUBMITNAME");
					name = in.readLine();
					if (name == null || name.equals(" ") || name.equals("")
							|| name.equals("null")) {
						return;
					}
					synchronized (names) {
						if (!names.contains(name) && !name.contains(" ")) {
							if (names.size() > 0) {
								String connectedUsers = "";
								String hatsString = "";
								for (String s : names) {
									ArrayList<Integer> tempList = hats.get(s);
									for (Integer i : tempList) {
										hatsString += i + "-";
									}
									gui.log("Hats " + hatsString);
									if (hatsString.length() > 0) {
										hatsString.substring(0,
												hatsString.length() - 1);

									}
									connectedUsers += s + "&" + xcoords.get(s)
											+ "&" + puppetNums.get(s) + "&"
											+ mouthNums.get(s) + "&"
											+ browNums.get(s) + "&"
											+ facings.get(s) + "&"
											+ bodies.get(s) + "&" + hatsString
											+ "&" + displayHands.get(s) + "&"
											+ currentHands.get(s) + "&"
											+ handFrames.get(s) + ",";
								}

								connectedUsers = "CONNECTEDUSERS: "
										+ connectedUsers.substring(0,
												connectedUsers.length() - 1);
								if (gui != null) {
									gui.log(connectedUsers);
								} else {
									System.out.println(connectedUsers);
								}
								out.println(connectedUsers);

							}
							names.add(name);
							xcoords.put(name, 0);
							puppetNums.put(name, 0);
							browNums.put(name, "0");
							mouthNums.put(name, "0");
							facings.put(name, Expressions.FACING_RIGHT);
							bodies.put(name, 0);
							hats.put(name, new ArrayList<Integer>());
							displayHands.put(name, "false");
							currentHands.put(name, "0");
							handFrames.put(name, "0");
							
							out.println("DIENUM " + dieNum);
							if (gui != null) {
								gui.log(name + " connected.");
							} else {
								System.out.println(name + " connected.");
							}
							break;
						}
					}
				}

				// Now that a successful name has been chosen, add the
				// socket's print writer to the set of all writers so
				// this client can receive broadcast messages.
				out.println("NAMEACCEPTED");
				for (PrintWriter writer : writers) {
					writer.println("RAW " + name + " connected.");
				}
				writers.add(out);
				// Add users to userlist in gui
				if (gui != null) {
					gui.clearUsers();
					for (String s : names) {
						gui.addUser(s);
					}
				}
				// Accept messages from this client and broadcast them.
				// Ignore other clients that cannot be broadcasted to.
				while (true) {
					boolean shouldLogLine = true;
					String input = in.readLine();
					if (input == null) {
						return;
					}
					if(input.contains("CONFIGSTRING")){
						String newConfig = input.substring(input.indexOf(" "));
						boolean mismatched = false;
						
						for (Map.Entry<String, String> entry : configStrings.entrySet()) {
						    String value = entry.getValue();
						    gui.log("stored config:"+value);
						    gui.log("new config"+newConfig);
						    if(!value.equals(newConfig)){
						    	mismatched = true;
						    }
						}
						if(mismatched){
							for (PrintWriter writer : writers) {
								writer.println(name + " " + "CONFIGMISMATCH");
							}
						}
						configStrings.put(name, newConfig);
						continue;
					}
					String[] inputs = input.split(" ");
					if (inputs.length > 1
							&& inputs[0].equalsIgnoreCase("XCOORD")) {
						xcoords.put(name, Integer.parseInt(inputs[1]));

					} else if (inputs.length > 1
							&& inputs[0].equalsIgnoreCase("PUPPETSWITCH")) {
						puppetNums.put(name, Integer.parseInt(inputs[1]));
					} else if (inputs.length > 1
							&& inputs[0].equalsIgnoreCase("ROLL")) {
						dieNum = (dieNum + 1) % 5;
						// wouldnt want the server host to know what the
						// roll is gonna be, that'd be no fun
						shouldLogLine = false;
						for (PrintWriter writer : writers) {
							writer.println(name + " " + input + " " + dieNum);
						}
					} else if (inputs.length > 1
							&& inputs[0].equalsIgnoreCase("FACING")) {
						if (inputs[1].equalsIgnoreCase("right")) {
							facings.put(name, Expressions.FACING_RIGHT);
						} else {
							facings.put(name, Expressions.FACING_LEFT);
						}

					} else if (inputs.length > 1
							&& inputs[0].equalsIgnoreCase("MOUTH")) {
						mouthNums.put(name, inputs[1]);
					} else if (inputs.length > 1
							&& inputs[0].equalsIgnoreCase("BROW")) {
						browNums.put(name, inputs[1]);
					} else if (inputs.length > 1
							&& inputs[0].equalsIgnoreCase("HATADD")) {
						ArrayList<Integer> temp = hats.get(name);
						temp.add(Integer.parseInt(inputs[1]));
					} else if (inputs.length > 1
							&& inputs[0].equalsIgnoreCase("HATREMOVE")) {
						ArrayList<Integer> temp = hats.get(name);
						temp.remove(new Integer(Integer.parseInt(inputs[1])));
					} else if (inputs.length > 1
							&& inputs[0].equalsIgnoreCase("HATS")) {

						String[] hatsToSet = inputs[1].split(",");
						ArrayList<Integer> temp = new ArrayList<Integer>();
						for (String s : hatsToSet) {

							temp.add(new Integer(Integer.parseInt(s)));

							hats.put(name, temp);
						}
					} else if (inputs.length > 1
							&& inputs[0].equalsIgnoreCase("BODY")) {
						bodies.put(name, Integer.parseInt(inputs[1]));
					}
					else if (inputs.length > 1
							&& inputs[0].equalsIgnoreCase("DISPLAYHAND")) {
						displayHands.put(name, inputs[1]);
					}
					else if (inputs.length > 1
							&& inputs[0].equalsIgnoreCase("HAND")) {
						currentHands.put(name, inputs[1]);
					}
					else if (inputs.length > 1
							&& inputs[0].equalsIgnoreCase("HANDFRAME")) {
						handFrames.put(name, inputs[1]);
					}
					
					// if the server should log the line or not
					// most of the output is spam, but some things
					// really shouldnt be seen, like rolls
					if (shouldLogLine) {
						if (gui != null) {
							gui.log(name + " " + input);
						} else {
							System.out.println(name + " " + input);
						}
						// broadcast to all connected clients

						for (PrintWriter writer : writers) {
							writer.println(name + " " + input);
						}

					}
					shouldLogLine = true;
				}
			} catch (IOException e) {
				if (gui != null) {
					gui.log(name + " disconnected.");
				} else {
					System.out.println(name + " disconnected.");
				}

			} finally {
				// This client is going down! Remove its name and its print
				// writer from the sets, and close its socket.
				if (name != null) {
					for (PrintWriter writer : writers) {
						writer.println("RAW " + name + ": quit.");
					}
					names.remove(name);
					puppetNums.remove(name);
					xcoords.remove(name);
					mouthNums.remove(name);
					browNums.remove(name);
					hats.remove(name);
					facings.remove(name);
					bodies.remove(name);
					displayHands.remove(name);
					currentHands.remove(name);
					handFrames.remove(name);
					configStrings.remove(name);
					if (gui != null) {
						gui.clearUsers();
						for (String s : names) {
							gui.addUser(s);
						}
					}
				}
				if (out != null) {
					writers.remove(out);
				}
				try {
					socket.close();
				} catch (IOException e) {
				}
			}
		}
	}
}
